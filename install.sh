chrts=$(cat names.txt | cut -d " " -f1)

for chart in $chrts;
do
	cp ../charts/$chart/values.yaml yamls/$chart.yaml
	chartname=$(cat names.txt | grep -i $chart | cut -d " " -f2)
	helm list | grep -i $chart
	if [ $(echo $?) = 0 ]
	then
		helm upgrade -n dev $chartname charts/$chart
	else
		helm install -n dev $chartname charts/$chart
	fi
done
